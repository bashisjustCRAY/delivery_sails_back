/**
 * HobbyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
 var AWS = require('aws-sdk');
 AWS.config.update({
  accessKeyId: process.env.AWS_KEY,
  secretAccessKey: process.env.AWS_ACCESS,
  region: 'us-east-1'
});
const ses = new AWS.SES();

const accountSid = process.env.TWILIO_SID;
const authToken = process.env.AUTH;
const twilioPhone = process.env.PHONE;
const client = require('twilio')(accountSid, authToken);

module.exports = {
  addhobby: async function (req, res) {
    var myhobby = req.body.myhobby.trim();
    var hobbyowner = req.body.hobbyowner.trim();

    if (myhobby !== undefined || hobbyowner !== undefined || myhobby.length === 0 || hobbyowner.length === 0) {
      await User.find({
        id: hobbyowner,
      });
      await Hobby.findOrCreate({
        myhobby: myhobby,
        owner: hobbyowner,
      }, {
        myhobby: myhobby,
        owner: hobbyowner,
      }, (error, existingHobby, newHobby) => {
        if (error) {
          return res.json({
            error: error,
          });
        } else if (newHobby) {
            const params = {
            Destination: {
              ToAddresses: [req.session.email]
            },
            ConfigurationSetName: 'Delivery-Science',
            Message: {
              Body: {
                Html: {
                 Charset: "UTF-8",
                 Data: "<html><body><h1>You have added a new Hobby</h1></body></html>"
                },
                Text: {
                 Charset: "UTF-8",
                 Data: "You have added a new Hobby"
                }
               },
               Subject: {
                Charset: 'UTF-8',
                Data: 'Delivery Science Hobby mail'
               }
              },
            Source: 'bashiralatishe@gmail.com'
          };
          const sendPromise = ses.sendEmail(params).promise();
          sendPromise.then(function(data) {
            console.log(data.MessageId);
          }).catch(function(err) {
            console.error(err, err.stack);
          });
          client.messages.create({
            body: 'You have added a new hobby',
            from: process.env.PHONE,
            to: req.session.phone
          }).then((message) => console.log(message.sid)).done();
          return res.status(200).json({
            message: 'You have added a new hobby',
          });
        } else {
          return res.json({
            message: 'Hobby Previously Added',
          });
        }
      });
    }
    else {
      return res.json({
        error: 'Incomplete Credentials',
      });
    }
  }
};
