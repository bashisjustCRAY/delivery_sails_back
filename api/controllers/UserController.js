/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var passport = require('passport');

module.exports = {
  signin: function (req, res) {
    passport.authenticate('local', (err, user, info) => {
      if ((err) || (!user)) {
        return res.send({
          message: info.message,
          authStatus: false,
        });
      }

      message = '';
      email = user.email;
      req.session.email = user.email;
      phone = user.phone;
      req.session.phone = user.phone;
      req.session.authenticated = true;
      return res.json({
        authStatus: true,
      });
    })(req, res);
  },

  signup: function (req, res) {
    User.findOrCreate({
      username: req.body.username,
      email: req.body.email
    }, {
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      username: req.body.username,
      email: req.body.email,
      phone: req.body.phone,
      password: req.body.password
    }, (err, existingUser, newUser) => {
      if (err) {
        return res.json({
          statusMessage: 'Check your datapage again',
        });
      }
      if (newUser) {
        return res.json({
          statusMessage: 'User Successfully added',
        });
      } else {
        return res.json({
          statusMessage: 'This user already exists',
        });
      }
      // res.redirect('/login');
    });
  }
};
